 [![pipeline status](https://gitlab.com/jaderfc/gaming_assessment_challenge/badges/master/pipeline.svg)](https://gitlab.com/jaderfc/gaming_assessment_challenge/-/commits/master)
# Gaming Assessment - Challenge

The project was made to cover the following scenarios below, and a job was created to run the scenarios.
- Bet +1 / +10 / 1/2 / X2 buttons work as expected
- Roll under/over switch changes value
- Dragging slider updates values inside inputs
- Updating inputs updates other inputs
- Updating Rolls count in Spray mode updates button text
- Profit on win is updated accordingly

### Important:
The project was designed to have clean / readable code and the tests are not fragile (without XPath, for example)

## Tools & Framework Used.
* Language: Javascript
* Tool: Cypress
* Version: 7.3.0

The scripts executes some test scenarios on the https://www.csgoroll.com/en/dice webpage.

## Setup
### Command to run
To setup and install the project, run the command;
* npm install

## Execution

You can run the project locally with
* `npx cypress open` if you want to run the whole test on the headed browser
* `npx cypress run` if you want to run the whole test on the headless mode
